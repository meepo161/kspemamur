package ru.avem.kspemamur.controllers;

import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import ru.avem.kspemamur.Main;
import ru.avem.kspemamur.communication.CommunicationModel;


public class CurrentProtectionWindowController extends CurrentProtection {

    @FXML
    private AnchorPane root;

    @FXML
    public void initialize() {
        Main.setTheme(root);
        CommunicationModel model = CommunicationModel.getInstance();
        model.addObserver(this);
        model.initOwenPRforCurrentProtection();
    }
}
