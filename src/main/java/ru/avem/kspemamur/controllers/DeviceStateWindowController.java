package ru.avem.kspemamur.controllers;

import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import ru.avem.kspemamur.communication.CommunicationModel;

import java.net.URISyntaxException;

import static ru.avem.kspemamur.Main.setTheme;

public class DeviceStateWindowController extends DeviceState {

    @FXML
    private AnchorPane root;

    @FXML
    public void initialize() throws URISyntaxException {
        setTheme(root);
        CommunicationModel model = CommunicationModel.getInstance();
        model.addObserver(this);
        model.setDeviceStateOn(true);
        model.setNeedToReadAllDevices(true);
    }
}