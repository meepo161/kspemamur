package ru.avem.kspemamur.states.main;

public interface State {
    void toIdleState();

    void toWaitState();

    void toResultState();
}
