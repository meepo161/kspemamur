package ru.avem.kspemamur.states.main;


public interface Statable {
    void toIdleState();

    void toWaitState();

    void toResultState();
}
