package ru.avem.kspemamur.communication.devices;

public interface DeviceController {
    int INPUT_BUFFER_SIZE = 256;
    byte NUMBER_OF_READ_ATTEMPTS = 5; //TODO 2
    byte NUMBER_OF_WRITE_ATTEMPTS = 5; //TODO 2

    byte NUMBER_OF_READ_ATTEMPTS_OF_ATTEMPTS = 5; //TODO 10
    byte NUMBER_OF_WRITE_ATTEMPTS_OF_ATTEMPTS = 5; //TODO 10

    int PM130_ID = 1;
    int PARMA400_ID = 2;
    int AVEM_ID = 3;
    int PHASEMETER_ID = 4;
    int IKAS_ID = 5;
    int PR200_ID = 6;
    int DELTACP2000_ID = 7;
    int TRM_ID = 8;

    void read(Object... args);

    boolean thereAreReadAttempts();

    void write(Object... args);

    boolean thereAreWriteAttempts();

    boolean needToRead();

    void setNeedToRead(boolean needToRead);

    void resetAllAttempts();

    void resetAllDeviceStateOnAttempts();
}